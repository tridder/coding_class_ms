package Project.CodingClassMS.mapper;

import Project.CodingClassMS.dto.CareerDTO;
import Project.CodingClassMS.dto.ClassDTO;
import Project.CodingClassMS.dto.FacultyDTO;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClassMapper implements RowMapper<ClassDTO> {
    @Override
    public ClassDTO map(ResultSet rs, StatementContext ctx) throws SQLException {
        String format = "yyyy-MM-dd";

        ClassDTO classDTO = new ClassDTO();
        classDTO.setId(rs.getInt("id"));
        classDTO.setName(rs.getString("name"));

        CareerDTO careerDTO = new CareerDTO();
        careerDTO.setId(rs.getInt("career_id"));
        careerDTO.setName(rs.getString("career_name"));
        classDTO.setCareerId(careerDTO);

        classDTO.setTotalDivisions(rs.getInt("total_divisions"));

        return classDTO;
    }
}