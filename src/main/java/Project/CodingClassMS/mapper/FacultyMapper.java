package Project.CodingClassMS.mapper;

import Project.CodingClassMS.dto.FacultyDTO;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FacultyMapper implements RowMapper<FacultyDTO> {
    @Override
    public FacultyDTO map(ResultSet rs, StatementContext ctx) throws SQLException {
        String format = "yyyy-MM-dd";

        FacultyDTO facultyDTO = new FacultyDTO();
        facultyDTO.setId(rs.getInt("id"));
        facultyDTO.setName(rs.getString("name"));

        return facultyDTO;
    }
}