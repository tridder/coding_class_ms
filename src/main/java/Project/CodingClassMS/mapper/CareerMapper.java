package Project.CodingClassMS.mapper;

import Project.CodingClassMS.dto.CareerDTO;
import Project.CodingClassMS.dto.FacultyDTO;
import Project.CodingClassMS.dto.UserDTO;
import Project.CodingClassMS.helper.AppHelper;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CareerMapper implements RowMapper<CareerDTO> {
    @Override
    public CareerDTO map(ResultSet rs, StatementContext ctx) throws SQLException {
        String format = "yyyy-MM-dd";

        CareerDTO careerDTO = new CareerDTO();
        careerDTO.setId(rs.getInt("id"));
        careerDTO.setName(rs.getString("name"));
        careerDTO.setArea(rs.getString("area"));

        FacultyDTO facultyDTO = new FacultyDTO();
        facultyDTO.setId(rs.getInt("faculty_id"));
        try{
            facultyDTO.setName(rs.getString("faculty_name"));
        }catch (Exception e){
            facultyDTO.setName(null);
        }
        careerDTO.setFacultyId(facultyDTO);

        careerDTO.setIsTSU(rs.getBoolean("is_TSU"));
        careerDTO.setTotalQuarters(rs.getInt("total_quarters"));

        CareerDTO careerDTO2 = new CareerDTO();
        careerDTO2.setId(rs.getInt("ing_id"));
        try{
            careerDTO2.setName(rs.getString("ing_name"));
        }catch (Exception e){
            careerDTO2.setName(null);
        }
        careerDTO.setIngId(careerDTO2);

        return careerDTO;
    }
}