package Project.CodingClassMS.mapper;

import Project.CodingClassMS.dto.*;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GradeMapper implements RowMapper<GradeDTO> {
    @Override
    public GradeDTO map(ResultSet rs, StatementContext ctx) throws SQLException {
        String format = "yyyy-MM-dd";

        GradeDTO gradeDTO = new GradeDTO();
        gradeDTO.setId(rs.getInt("id"));

        ClassDTO classDTO = new ClassDTO();
        classDTO.setId(rs.getInt("class_id"));
        classDTO.setName(rs.getString("class_name"));
        gradeDTO.setClassId(classDTO);

        UserDTO studentDTO = new UserDTO();
        studentDTO.setId(rs.getInt("student_id"));
        studentDTO.setName(rs.getString("student_name"));
        gradeDTO.setStudentId(studentDTO);

        gradeDTO.setScore1(rs.getInt("score1"));
        gradeDTO.setScore2(rs.getInt("score2"));
        gradeDTO.setScore3(rs.getInt("score3"));

        return gradeDTO;
    }
}