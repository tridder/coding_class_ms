package Project.CodingClassMS.mapper;

import Project.CodingClassMS.dto.CareerDTO;
import Project.CodingClassMS.dto.GroupDTO;
import Project.CodingClassMS.dto.UserDTO;
import Project.CodingClassMS.helper.AppHelper;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupMapper implements RowMapper<GroupDTO> {
    @Override
    public GroupDTO map(ResultSet rs, StatementContext ctx) throws SQLException {
        String format = "yyyy-MM-dd";

        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setId(rs.getInt("id"));
        groupDTO.setName(rs.getString("name"));

        CareerDTO careerDTO = new CareerDTO();
        careerDTO.setId(rs.getInt("career_id"));
        try{
            careerDTO.setName(rs.getString("career_name"));
        }catch (Exception e){
            careerDTO.setName(null);
        }
        groupDTO.setCareerId(careerDTO);

        groupDTO.setStartDate(AppHelper.stringToDate(rs.getString("start_date"), format));
        groupDTO.setEndDate(AppHelper.stringToDate(rs.getString("end_date"), format));

        return groupDTO;
    }
}