package Project.CodingClassMS.mapper;

import Project.CodingClassMS.dto.*;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentGroupMapper implements RowMapper<StudentGroupDTO> {
    @Override
    public StudentGroupDTO map(ResultSet rs, StatementContext ctx) throws SQLException {
        String format = "yyyy-MM-dd";

        StudentGroupDTO studentGroupDTO = new StudentGroupDTO();
        studentGroupDTO.setId(rs.getInt("id"));
        studentGroupDTO.setName(rs.getString("name"));

        UserDTO studentDTO = new UserDTO();
        studentDTO.setId(rs.getInt("student_id"));
        studentDTO.setName(rs.getString("student_name"));
        studentGroupDTO.setStudentId(studentDTO);

        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setId(rs.getInt("group_id"));
        groupDTO.setName(rs.getString("group_name"));
        studentGroupDTO.setGroupId(groupDTO);

        return studentGroupDTO;
    }
}