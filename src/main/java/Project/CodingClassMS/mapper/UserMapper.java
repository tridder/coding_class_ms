package Project.CodingClassMS.mapper;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import Project.CodingClassMS.dto.*;
import Project.CodingClassMS.helper.AppHelper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<UserDTO> {
    @Override
    public UserDTO map(ResultSet rs, StatementContext ctx) throws SQLException {
        String format = "yyyy-MM-dd";

        UserDTO userDTO = new UserDTO();
        userDTO.setId(rs.getInt("id"));
        userDTO.setUsername(rs.getString("username"));
        userDTO.setPassword(rs.getString("password"));
        userDTO.setStatus(rs.getString("status"));
        userDTO.setName(rs.getString("name"));
        userDTO.setLastName(rs.getString("last_name"));
        userDTO.setBirthDate(AppHelper.stringToDate(rs.getString("birth_date"), format));

        LevelDTO levelDTO = new LevelDTO();
        levelDTO.setId(rs.getInt("level_id"));
        try{
            levelDTO.setName(rs.getString("level_name"));
        }catch (Exception e){
            levelDTO.setName(null);
        }
        userDTO.setLevelId(levelDTO);

        userDTO.setPhoto(rs.getString("photo"));
        if(rs.getString("hire_date") != null){
            userDTO.setHireDate(AppHelper.stringToDate(rs.getString("hire_date"), format));
        }else{
            userDTO.setHireDate(null);
        }

        return userDTO;
    }
}