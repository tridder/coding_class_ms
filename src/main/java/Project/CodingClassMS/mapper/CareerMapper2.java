package Project.CodingClassMS.mapper;

import Project.CodingClassMS.dto.CareerDTO;
import Project.CodingClassMS.dto.FacultyDTO;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CareerMapper2 implements RowMapper<CareerDTO> {
    @Override
    public CareerDTO map(ResultSet rs, StatementContext ctx) throws SQLException {
        String format = "yyyy-MM-dd";

        CareerDTO careerDTO = new CareerDTO();
        careerDTO.setId(rs.getInt("id"));
        careerDTO.setName(rs.getString("name"));
        careerDTO.setArea(rs.getString("area"));

        return careerDTO;
    }
}