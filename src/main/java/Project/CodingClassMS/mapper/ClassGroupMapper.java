package Project.CodingClassMS.mapper;

import Project.CodingClassMS.dto.*;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClassGroupMapper implements RowMapper<ClassGroupDTO> {
    @Override
    public ClassGroupDTO map(ResultSet rs, StatementContext ctx) throws SQLException {
        String format = "yyyy-MM-dd";

        ClassGroupDTO classGroupDTO = new ClassGroupDTO();
        classGroupDTO.setId(rs.getInt("id"));
        classGroupDTO.setName(rs.getString("name"));

        ClassDTO classDTO = new ClassDTO();
        classDTO.setId(rs.getInt("class_id"));
        classDTO.setName(rs.getString("class_name"));
        classGroupDTO.setClassId(classDTO);

        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setId(rs.getInt("group_id"));
        groupDTO.setName(rs.getString("group_name"));
        classGroupDTO.setGroupId(groupDTO);

        UserDTO teacherDTO = new UserDTO();
        teacherDTO.setId(rs.getInt("teacher_id"));
        teacherDTO.setName(rs.getString("teacher_name"));
        classGroupDTO.setTeacherId(teacherDTO);

        return classGroupDTO;
    }
}