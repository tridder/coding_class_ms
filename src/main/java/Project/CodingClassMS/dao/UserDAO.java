package Project.CodingClassMS.dao;

import Project.CodingClassMS.helper.AppHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import Project.CodingClassMS.dto.UserDTO;
import Project.CodingClassMS.mapper.UserMapper;
import org.mindrot.jbcrypt.BCrypt;

import java.io.File;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

@Repository
public class UserDAO {

    private Jdbi jdbi;

    @Autowired
    public UserDAO(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    public int insert(UserDTO userDTO) throws Exception {
        try (Handle handle = jdbi.open()) {
            String sql = "INSERT INTO user VALUES (null, :username, :password, :status, :name, :lastName, :birthDate, :levelId.id, :photo, :hireDate)";

            return handle.createUpdate(sql)
                    .bindBean(userDTO)
                    .executeAndReturnGeneratedKeys()
                    .mapTo(int.class)
                    .findOnly();

        }
    }

    public boolean update(UserDTO userDTO) throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "UPDATE user SET username = :username, password = :password, name = :name, " +
                    "last_name = :lastName, birth_date = :birthDate, photo = :photo, hire_date = :hireDate WHERE id = :id";

            return handle.createUpdate(sql)
                    .bindBean(userDTO)
                    .execute() == 1;
        }
    }

    public List<UserDTO> select(int type) throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT * FROM user WHERE level_id ="+type;
            return handle.createQuery(sql)
                    .map(new UserMapper())
                    .list();
        }
    }

    public UserDTO search(int id) throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT u.* FROM user u WHERE id = :id";
            return handle.createQuery(sql)
                    .bind("id", id)
                    .map(new UserMapper())
                    .findOnly();
        }
    }

    public Map<String, Object> exist(UserDTO userDTO, String parameter) throws Exception{
        try (Handle handle = jdbi.open()) {
            Map<String, Object> response = new HashMap<>();
            response.put("username", "OK");
            String sql = "SELECT * FROM user WHERE";

            if(parameter.equals("update")){
                sql += " id != :id AND";
            }

            Optional username = handle.createQuery(sql + " username = :username")
                    .bindBean(userDTO)
                    .map(new UserMapper())
                    .findFirst();
            if(username.isPresent()){
                response.put("username", "EXIST");
            }

            return response;
        }
    }

    public UserDTO existId(int id) throws Exception {
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT u.*,(SELECT l.name FROM level l WHERE l.id = u.level_id) level_name FROM user u WHERE id = :id";
            return handle.createQuery(sql)
                    .bind("id", id)
                    .map(new UserMapper())
                    .findOnly();
        }
    }

    public UserDTO login(String username, String password) throws Exception {
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT u.*,(SELECT l.name FROM level l WHERE l.id = u.level_id) level_name FROM user u WHERE u.username = :username";
            if (handle.createQuery(sql)
                    .bind("username", username)
                    .map(new UserMapper())
                    .findFirst().isPresent()) {
                String sql2 = "SELECT * FROM user WHERE username = :username AND status = 1";
                if (handle.createQuery(sql2)
                        .bind("username", username)
                        .map(new UserMapper())
                        .findFirst().isPresent()) {
                    UserDTO userDTO = handle.createQuery(sql)
                            .bind("username", username)
                            .map(new UserMapper())
                            .findOnly();
                    if (BCrypt.checkpw(password, userDTO.getPassword())) {
                        return userDTO;
                    } else {
                        UserDTO user = new UserDTO();
                        user.setId(11111111);
                        return user;
                    }
                } else{
                    UserDTO user = new UserDTO();
                    user.setId(33333333);
                    return user;
                }
            } else {
                UserDTO user = new UserDTO();
                user.setId(22222222);
                return user;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}