package Project.CodingClassMS.dao;

import Project.CodingClassMS.dto.CareerDTO;
import Project.CodingClassMS.dto.GroupDTO;
import Project.CodingClassMS.mapper.CareerMapper;
import Project.CodingClassMS.mapper.CareerMapper2;
import Project.CodingClassMS.mapper.GroupMapper;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class GroupDAO {

    private Jdbi jdbi;

    @Autowired
    public GroupDAO(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    public int insert(GroupDTO groupDTO) throws Exception {
        try (Handle handle = jdbi.open()) {
            String sql = "INSERT INTO `group` VALUES (null, :name, :careerId.id, :startDate, :endDate)";

            return handle.createUpdate(sql)
                    .bindBean(groupDTO)
                    .executeAndReturnGeneratedKeys()
                    .mapTo(int.class)
                    .findOnly();

        }
    }

    public boolean update(GroupDTO groupDTO) throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "UPDATE `group` SET name = :name, career_id = :careerId.id, start_date = :startDate, end_date = :endDate WHERE id = :id";

            return handle.createUpdate(sql)
                    .bindBean(groupDTO)
                    .execute() == 1;
        }
    }

    public GroupDTO search(int id) throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT g.* FROM `group` g WHERE id = :id";
            return handle.createQuery(sql)
                    .bind("id", id)
                    .map(new GroupMapper())
                    .findOnly();
        }
    }

    public List<GroupDTO> select() throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT g.*, (SELECT c.name FROM career c WHERE c.id = g.career_id) career_name FROM `group` g";
            return handle.createQuery(sql)
                    .map(new GroupMapper())
                    .list();
        }
    }

    public Map<String, Object> exist(GroupDTO groupDTO, String parameter) throws Exception{
        try (Handle handle = jdbi.open()) {
            Map<String, Object> response = new HashMap<>();
            response.put("name", "OK");
            String sql = "SELECT * FROM `group` WHERE";

            if(parameter.equals("update")){
                sql += " id != :id AND";
            }

            Optional name = handle.createQuery(sql + " name = :name")
                    .bindBean(groupDTO)
                    .map(new GroupMapper())
                    .findFirst();
            if(name.isPresent()){
                response.put("name", "EXIST");
            }

            return response;
        }
    }

    public GroupDTO existId(int id) throws Exception {
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT * FROM `group` WHERE id = :id";
            return handle.createQuery(sql)
                    .bind("id", id)
                    .map(new GroupMapper())
                    .findOnly();
        }
    }
}