package Project.CodingClassMS.dao;

import Project.CodingClassMS.dto.CareerDTO;
import Project.CodingClassMS.dto.FacultyDTO;
import Project.CodingClassMS.mapper.CareerMapper;
import Project.CodingClassMS.mapper.CareerMapper2;
import Project.CodingClassMS.mapper.FacultyMapper;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class FacultyDAO {

    private Jdbi jdbi;

    @Autowired
    public FacultyDAO(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    public int insert(FacultyDTO facultyDTO) throws Exception {
        try (Handle handle = jdbi.open()) {
            String sql = "INSERT INTO faculty VALUES (null, :name)";
            return handle.createUpdate(sql)
                    .bindBean(facultyDTO)
                    .executeAndReturnGeneratedKeys()
                    .mapTo(int.class)
                    .findOnly();

        }
    }

    public boolean update(FacultyDTO facultyDTO) throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "UPDATE faculty SET name = :name WHERE id = :id";

            return handle.createUpdate(sql)
                    .bindBean(facultyDTO)
                    .execute() == 1;
        }
    }

    public FacultyDTO search(int id) throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT f.* FROM faculty f WHERE id = :id";
            return handle.createQuery(sql)
                    .bind("id", id)
                    .map(new FacultyMapper())
                    .findOnly();
        }
    }

    public List<FacultyDTO> select() throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT * FROM faculty";
            return handle.createQuery(sql)
                    .map(new FacultyMapper())
                    .list();
        }
    }

    public Map<String, Object> exist(FacultyDTO facultyDTO, String parameter) throws Exception{
        try (Handle handle = jdbi.open()) {
            Map<String, Object> response = new HashMap<>();
            response.put("name", "OK");
            String sql = "SELECT * FROM faculty WHERE";

            if(parameter.equals("update")){
                sql += " id != :id AND";
            }

            Optional name = handle.createQuery(sql + " name = :name")
                    .bindBean(facultyDTO)
                    .map(new FacultyMapper())
                    .findFirst();
            if(name.isPresent()){
                response.put("name", "EXIST");
            }

            return response;
        }
    }

    public FacultyDTO existId(int id) throws Exception {
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT * FROM faculty WHERE id = :id";
            return handle.createQuery(sql)
                    .bind("id", id)
                    .map(new FacultyMapper())
                    .findOnly();
        }
    }
}