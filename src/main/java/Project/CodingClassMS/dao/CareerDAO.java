package Project.CodingClassMS.dao;

import Project.CodingClassMS.dto.CareerDTO;
import Project.CodingClassMS.dto.UserDTO;
import Project.CodingClassMS.mapper.CareerMapper;
import Project.CodingClassMS.mapper.CareerMapper2;
import Project.CodingClassMS.mapper.UserMapper;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class CareerDAO {

    private Jdbi jdbi;

    @Autowired
    public CareerDAO(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    public int insert(CareerDTO careerDTO) throws Exception {
        try (Handle handle = jdbi.open()) {
            String sql = "INSERT INTO career VALUES (null, :name, :area, :facultyId.id, :isTSU, :totalQuarters";

            if(careerDTO.getIngId().getId() == 0){
                sql += ", null)";
            }else{
                sql += ", :ingId.id)";
            }
            return handle.createUpdate(sql)
                    .bindBean(careerDTO)
                    .executeAndReturnGeneratedKeys()
                    .mapTo(int.class)
                    .findOnly();

        }
    }

    public boolean update(CareerDTO careerDTO) throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "UPDATE career SET name = :name, area = :area, faculty_id = :facultyId.id, is_TSU = :isTSU, total_quarters = :totalQuarters WHERE id = :id";

            return handle.createUpdate(sql)
                    .bindBean(careerDTO)
                    .execute() == 1;
        }
    }

    public CareerDTO search(int id) throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT c.*," +
                    "(SELECT name FROM faculty f WHERE f.id = c.faculty_id) faculty_name, " +
                    "(SELECT name FROM career ca WHERE ca.id = c.ing_id) ing_name " +
                    "FROM career c WHERE id = :id";
            return handle.createQuery(sql)
                    .bind("id", id)
                    .map(new CareerMapper())
                    .findOnly();
        }
    }

    public List<CareerDTO> select() throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT c.*," +
                    "(SELECT f.name FROM faculty f WHERE f.id = c.faculty_id) as faculty_name," +
                    "(SELECT ca.name FROM career ca WHERE ca.id = c.ing_id) as ing_name " +
                    "FROM career c";
            return handle.createQuery(sql)
                    .map(new CareerMapper())
                    .list();
        }
    }

    public List<CareerDTO> selectEng() throws Exception{
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT *,name faculty_name,name ing_name FROM career c WHERE is_TSU = false";
            return handle.createQuery(sql)
                    .map(new CareerMapper())
                    .list();
        }
    }

    public Map<String, Object> exist(CareerDTO careerDTO, String parameter) throws Exception{
        try (Handle handle = jdbi.open()) {
            Map<String, Object> response = new HashMap<>();
            response.put("namearea", "OK");
            String sql = "SELECT * FROM career WHERE";

            if(parameter.equals("update")){
                sql += " id != :id AND";
            }

            Optional namearea = handle.createQuery(sql + " name = :name AND area = :area")
                    .bindBean(careerDTO)
                    .map(new CareerMapper2())
                    .findFirst();
            if(namearea.isPresent()){
                response.put("namearea", "EXIST");
            }

            return response;
        }
    }

    public CareerDTO existId(int id) throws Exception {
        try (Handle handle = jdbi.open()) {
            String sql = "SELECT * FROM career WHERE id = :id";
            return handle.createQuery(sql)
                    .bind("id", id)
                    .map(new CareerMapper2())
                    .findOnly();
        }
    }
}