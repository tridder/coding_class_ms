package Project.CodingClassMS.controller;

import Project.CodingClassMS.dao.CareerDAO;
import Project.CodingClassMS.dao.FacultyDAO;
import Project.CodingClassMS.dao.GroupDAO;
import Project.CodingClassMS.dto.CareerDTO;
import Project.CodingClassMS.dto.GroupDTO;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/group")
public class GroupController {

    @Value("${code.OK}")
    private String OK;
    @Value("${code.NOK}")
    private String NOK;
    @Value("${code.ERROR}")
    private String ERROR;
    @Value("${code.EXIST}")
    private String EXIST;
    @Value("${code.NEXIST}")
    private String NEXIST;
    private final CareerDAO careerDAO;
    private final GroupDAO groupDAO;
    String secretKey = "mySecretKey";

    @Autowired
    public GroupController(CareerDAO careerDAO, GroupDAO groupDAO) {
        this.careerDAO = careerDAO;
        this.groupDAO = groupDAO;
    }

    @RequestMapping("/insert")
    public ResponseEntity<Map> insert(@RequestBody GroupDTO groupDTO, @RequestHeader Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response = groupDAO.exist(groupDTO,"insert");
            if (response.get("name").equals("OK")) {
                response = new HashMap<>();
                int id = Integer.parseInt(Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(request.get("authorization").toString().substring(6)).getBody().get("jti").toString());
                groupDAO.insert(groupDTO);
                response.put("code", OK);
            }else{
                response.put("code", EXIST);
            }
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/update")
    public ResponseEntity<Map> update(@RequestBody GroupDTO groupDTO,@RequestHeader Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            GroupDTO groupId = groupDAO.existId(groupDTO.getId());
            if (groupId != null) {
                response = groupDAO.exist(groupDTO,"update");
                if (response.get("name").equals("OK")) {
                    response = new HashMap<>();
                    int id = Integer.parseInt(Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(request.get("authorization").toString().substring(6)).getBody().get("jti").toString());
                    if(groupDAO.update(groupDTO)){
                        response.put("data", groupDTO);
                        response.put("code", OK);
                    }else{
                        response.put("code", NOK);
                    }
                }else{
                    response.put("code", EXIST);
                }
            }else{
                response.put("code", NEXIST);
            }
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/select")
    public ResponseEntity<Map> select(){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("groups",groupDAO.select());
            response.put("code", OK);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/search")
    public ResponseEntity<Map> search(@RequestBody Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("code", OK);
            Map<String, Object> data = new HashMap<>();
            data.put("group", groupDAO.search(Integer.parseInt(request.get("id").toString())));
            data.put("careers",careerDAO.select());

            response.put("data",data);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/new-group")
    public ResponseEntity<Map> newGroup(){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("code", OK);
            Map<String, Object> data = new HashMap<>();
            data.put("careers",careerDAO.select());

            response.put("data",data);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}