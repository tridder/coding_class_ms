package Project.CodingClassMS.controller;

import Project.CodingClassMS.dao.CareerDAO;
import Project.CodingClassMS.dao.FacultyDAO;
import Project.CodingClassMS.dto.CareerDTO;
import Project.CodingClassMS.dto.UserDTO;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/career")
public class CareerController {

    @Value("${code.OK}")
    private String OK;
    @Value("${code.NOK}")
    private String NOK;
    @Value("${code.ERROR}")
    private String ERROR;
    @Value("${code.EXIST}")
    private String EXIST;
    @Value("${code.NEXIST}")
    private String NEXIST;
    private final CareerDAO careerDAO;
    private final FacultyDAO facultyDAO;
    String secretKey = "mySecretKey";

    @Autowired
    public CareerController(CareerDAO careerDAO, FacultyDAO facultyDAO) {
        this.careerDAO = careerDAO;
        this.facultyDAO = facultyDAO;
    }

    @RequestMapping("/insert")
    public ResponseEntity<Map> insert(@RequestBody CareerDTO careerDTO, @RequestHeader Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response = careerDAO.exist(careerDTO,"insert");
            if (response.get("namearea").equals("OK")) {
                response = new HashMap<>();
                int id = Integer.parseInt(Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(request.get("authorization").toString().substring(6)).getBody().get("jti").toString());
                careerDAO.insert(careerDTO);
                response.put("code", OK);
            }else{
                response.put("code", EXIST);
            }
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/update")
    public ResponseEntity<Map> update(@RequestBody CareerDTO careerDTO,@RequestHeader Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            CareerDTO careerId = careerDAO.existId(careerDTO.getId());
            if (careerId != null) {
                response = careerDAO.exist(careerDTO,"update");
                if (response.get("namearea").equals("OK")) {
                    response = new HashMap<>();
                    int id = Integer.parseInt(Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(request.get("authorization").toString().substring(6)).getBody().get("jti").toString());
                    if(careerDAO.update(careerDTO)){
                        response.put("data", careerDTO);
                        response.put("code", OK);
                    }else{
                        response.put("code", NOK);
                    }
                }else{
                    response.put("code", EXIST);
                }
            }else{
                response.put("code", NEXIST);
            }
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/select")
    public ResponseEntity<Map> select(){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("careers",careerDAO.select());
            response.put("code", OK);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/search")
    public ResponseEntity<Map> search(@RequestBody Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("code", OK);
            Map<String, Object> data = new HashMap<>();
            data.put("career", careerDAO.search(Integer.parseInt(request.get("id").toString())));
            data.put("faculties",facultyDAO.select());
            data.put("engineering",careerDAO.selectEng());

            response.put("data",data);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/new-career")
    public ResponseEntity<Map> newCareer(){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("code", OK);
            Map<String, Object> data = new HashMap<>();
            data.put("faculties",facultyDAO.select());
            data.put("engineering",careerDAO.selectEng());

            response.put("data",data);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}