package Project.CodingClassMS.controller;

import Project.CodingClassMS.dto.UserDTO;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.*;
import Project.CodingClassMS.dao.UserDAO;
import Project.CodingClassMS.helper.AppHelper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/account")
public class AccountController {

    @Value("${code.OK}")
    private String OK;
    @Value("${code.NOK}")
    private String NOK;
    @Value("${code.ERROR}")
    private String ERROR;
    @Value("${code.EXIST}")
    private String EXIST;
    @Value("${code.NEXIST}")
    private String NEXIST;
    private final UserDAO userDAO;
    String secretKey = "mySecretKey";

    @Autowired
    public AccountController(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @RequestMapping("/login")
    public ResponseEntity<Map> login(@RequestBody Map<String, Object> request){
        String username = request.get("username").toString();
        String pass = request.get("password").toString();
        System.out.println(BCrypt.hashpw(AppHelper.decrypt(pass), BCrypt.gensalt(6)));
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            UserDTO user = userDAO.login(username, AppHelper.decrypt(pass));
            if (user.getLastName() != null) {
                response.put("code", OK);
                response.put("access_token", getJWTToken(username,user.getId()));
                response.put("user",AppHelper.profile(user));

            }else {
                response.put("code", NOK);
                if(user.getId() == 11111111){
                    Map<String, Object> error = new HashMap<>();
                    error.put("password", "Check Password");
                    response.put("error", error);
                }
                else if(user.getId() == 22222222){
                    Map<String, Object> error = new HashMap<>();
                    error.put("username", "Check Username");
                    response.put("error", error);
                }
                else if(user.getId() == 33333333){
                    Map<String, Object> error = new HashMap<>();
                    error.put("username", "Your account is inactive");
                    error.put("password", "Check with the administrator");
                    response.put("error", error);
                }
            }
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/token-access")
    public ResponseEntity<Map> tokenAccess(@RequestBody Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            int id = Integer.parseInt(Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(request.get("access_token").toString()).getBody().get("jti").toString());
            String username = Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(request.get("access_token").toString()).getBody().get("sub").toString();

            UserDTO user = userDAO.existId(id);
            if (user != null) {
                response.put("code", OK);
                response.put("access_token", getJWTToken(username,id));
                response.put("user",AppHelper.profile(user));
            }else {
                response.put("code", NOK);
            }
        } catch (Exception e) {
            response.put("error", "Invalid automatic access, try login again");
//            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private String getJWTToken(String username,int id) {
        String secretKey = "mySecretKey";
        
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId(String.valueOf(id))
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000000))
                .signWith(SignatureAlgorithm.HS512,secretKey.getBytes()).compact();

        return token;
    }

}