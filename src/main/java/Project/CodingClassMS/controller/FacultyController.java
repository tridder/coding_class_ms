package Project.CodingClassMS.controller;

import Project.CodingClassMS.dao.CareerDAO;
import Project.CodingClassMS.dao.FacultyDAO;
import Project.CodingClassMS.dto.CareerDTO;
import Project.CodingClassMS.dto.FacultyDTO;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/faculty")
public class FacultyController {

    @Value("${code.OK}")
    private String OK;
    @Value("${code.NOK}")
    private String NOK;
    @Value("${code.ERROR}")
    private String ERROR;
    @Value("${code.EXIST}")
    private String EXIST;
    @Value("${code.NEXIST}")
    private String NEXIST;
    private final FacultyDAO facultyDAO;
    String secretKey = "mySecretKey";

    @Autowired
    public FacultyController(FacultyDAO facultyDAO) {
        this.facultyDAO = facultyDAO;
    }

    @RequestMapping("/insert")
    public ResponseEntity<Map> insert(@RequestBody FacultyDTO facultyDTO, @RequestHeader Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response = facultyDAO.exist(facultyDTO,"insert");
            if (response.get("name").equals("OK")) {
                response = new HashMap<>();
                int id = Integer.parseInt(Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(request.get("authorization").toString().substring(6)).getBody().get("jti").toString());
                facultyDAO.insert(facultyDTO);
                response.put("code", OK);
            }else{
                response.put("code", EXIST);
            }
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/update")
    public ResponseEntity<Map> update(@RequestBody FacultyDTO facultyDTO,@RequestHeader Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            FacultyDTO facultyId = facultyDAO.existId(facultyDTO.getId());
            if (facultyId != null) {
                response = facultyDAO.exist(facultyDTO,"update");
                if (response.get("name").equals("OK")) {
                    response = new HashMap<>();
                    int id = Integer.parseInt(Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(request.get("authorization").toString().substring(6)).getBody().get("jti").toString());
                    if(facultyDAO.update(facultyDTO)){
                        response.put("data", facultyDTO);
                        response.put("code", OK);
                    }else{
                        response.put("code", NOK);
                    }
                }else{
                    response.put("code", EXIST);
                }
            }else{
                response.put("code", NEXIST);
            }
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/select")
    public ResponseEntity<Map> select(){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("faculties",facultyDAO.select());
            response.put("code", OK);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/search")
    public ResponseEntity<Map> search(@RequestBody Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("code", OK);
            Map<String, Object> data = new HashMap<>();
            data.put("faculty", facultyDAO.search(Integer.parseInt(request.get("id").toString())));

            response.put("data",data);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/new-faculty")
    public ResponseEntity<Map> newCareer(){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("code", OK);
            Map<String, Object> data = new HashMap<>();

            response.put("data",data);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}