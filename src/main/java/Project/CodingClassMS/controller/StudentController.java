package Project.CodingClassMS.controller;

import Project.CodingClassMS.dao.UserDAO;
import Project.CodingClassMS.dto.UserDTO;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/student")
public class StudentController {

    @Value("${code.OK}")
    private String OK;
    @Value("${code.NOK}")
    private String NOK;
    @Value("${code.ERROR}")
    private String ERROR;
    @Value("${code.EXIST}")
    private String EXIST;
    @Value("${code.NEXIST}")
    private String NEXIST;
    private final UserDAO userDAO;
    String secretKey = "mySecretKey";

    @Autowired
    public StudentController(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @RequestMapping("/insert")
    public ResponseEntity<Map> insert(@RequestBody UserDTO studentDTO, @RequestHeader Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response = userDAO.exist(studentDTO,"insert");
            if (response.get("username").equals("OK")) {
                response = new HashMap<>();
                int id = Integer.parseInt(Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(request.get("authorization").toString().substring(6)).getBody().get("jti").toString());
                userDAO.insert(studentDTO);
                response.put("code", OK);
            }else{
                response.put("code", EXIST);
            }
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/update")
    public ResponseEntity<Map> update(@RequestBody UserDTO studentDTO,@RequestHeader Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            UserDTO studentId = userDAO.existId(studentDTO.getId());
            if (studentId != null) {
                response = userDAO.exist(studentDTO,"update");
                if (response.get("username").equals("OK")) {
                    response = new HashMap<>();
                    int id = Integer.parseInt(Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(request.get("authorization").toString().substring(6)).getBody().get("jti").toString());
                    if(userDAO.update(studentDTO)){
                        response.put("data", studentDTO);
                        response.put("code", OK);
                    }else{
                        response.put("code", NOK);
                    }
                }else{
                    response.put("code", EXIST);
                }
            }else{
                response.put("code", NEXIST);
            }
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/select")
    public ResponseEntity<Map> select(){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("students",userDAO.select(3));
            response.put("code", OK);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/search")
    public ResponseEntity<Map> search(@RequestBody Map<String, Object> request){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("code", OK);
            Map<String, Object> data = new HashMap<>();
            data.put("student", userDAO.search(Integer.parseInt(request.get("id").toString())));

            response.put("data",data);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping("/new-student")
    public ResponseEntity<Map> newStudent(){
        Map<String, Object> response = new HashMap<>();
        response.put("code", NOK);
        try {
            response.put("code", OK);
            Map<String, Object> data = new HashMap<>();

            response.put("data",data);
        } catch (Exception e) {
            response.put("code", ERROR + e);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}