package Project.CodingClassMS.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class CareerDTO {
    private int id;
    private String name;
    private String area;
    private FacultyDTO facultyId;
    private boolean isTSU;
    private int totalQuarters;
    private CareerDTO ingId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public FacultyDTO getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(FacultyDTO facultyId) {
        this.facultyId = facultyId;
    }

    public boolean getIsTSU() {
        return isTSU;
    }

    public void setIsTSU(boolean isTSU) {
        this.isTSU = isTSU;
    }

    public int getTotalQuarters() {
        return totalQuarters;
    }

    public void setTotalQuarters(int totalQuarters) {
        this.totalQuarters = totalQuarters;
    }

    public CareerDTO getIngId() {
        return ingId;
    }

    public void setIngId(CareerDTO ingId) {
        this.ingId = ingId;
    }
}