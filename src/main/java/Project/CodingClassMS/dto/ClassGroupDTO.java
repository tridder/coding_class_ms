package Project.CodingClassMS.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ClassGroupDTO {
    private int id;
    private String name;
    private ClassDTO classId;
    private GroupDTO groupId;
    private UserDTO teacherId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ClassDTO getClassId() {
        return classId;
    }

    public void setClassId(ClassDTO classId) {
        this.classId = classId;
    }

    public GroupDTO getGroupId() {
        return groupId;
    }

    public void setGroupId(GroupDTO groupId) {
        this.groupId = groupId;
    }

    public UserDTO getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(UserDTO teacherId) {
        this.teacherId = teacherId;
    }
}