package Project.CodingClassMS.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ClassDTO {
    private int id;
    private String name;
    private CareerDTO careerId;
    private int totalDivisions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CareerDTO getCareerId() {
        return careerId;
    }

    public void setCareerId(CareerDTO careerId) {
        this.careerId = careerId;
    }

    public int getTotalDivisions() {
        return totalDivisions;
    }

    public void setTotalDivisions(int totalDivisions) {
        this.totalDivisions = totalDivisions;
    }
}