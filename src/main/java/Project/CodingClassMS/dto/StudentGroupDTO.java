package Project.CodingClassMS.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class StudentGroupDTO {
    private int id;
    private String name;
    private UserDTO studentId;
    private GroupDTO groupId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDTO getStudentId() {
        return studentId;
    }

    public void setStudentId(UserDTO studentId) {
        this.studentId = studentId;
    }

    public GroupDTO getGroupId() {
        return groupId;
    }

    public void setGroupId(GroupDTO groupId) {
        this.groupId = groupId;
    }
}