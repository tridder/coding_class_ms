package Project.CodingClassMS.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

public class UserDTO {
    private int id;
    private String username;
    private String password;
    private String status;
    private String name;
    private String lastName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;
    private LevelDTO levelId;
    private String photo;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date hireDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public LevelDTO getLevelId() {
        return levelId;
    }

    public void setLevelId(LevelDTO levelId) {
        this.levelId = levelId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }
}