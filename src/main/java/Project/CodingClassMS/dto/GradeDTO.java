package Project.CodingClassMS.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class GradeDTO {
    private int id;
    private ClassDTO classId;
    private UserDTO studentId;
    private float score1;
    private float score2;
    private float score3;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ClassDTO getClassId() {
        return classId;
    }

    public void setClassId(ClassDTO classId) {
        this.classId = classId;
    }

    public UserDTO getStudentId() {
        return studentId;
    }

    public void setStudentId(UserDTO studentId) {
        this.studentId = studentId;
    }

    public float getScore1() {
        return score1;
    }

    public void setScore1(float score1) {
        this.score1 = score1;
    }

    public float getScore2() {
        return score2;
    }

    public void setScore2(float score2) {
        this.score2 = score2;
    }

    public float getScore3() {
        return score3;
    }

    public void setScore3(float score3) {
        this.score3 = score3;
    }
}