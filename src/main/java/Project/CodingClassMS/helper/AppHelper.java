package Project.CodingClassMS.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import Project.CodingClassMS.dto.UserDTO;
//import jdk.nashorn.internal.parser.JSONParser;
import org.apache.tomcat.util.codec.binary.Base64;
//import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class AppHelper {

    private static final String KEY = "3456789123456789";
    private static final String VECTOR = "4526589565236598";

    public static Date stringToDate(String date, String format){
        try {
            return new SimpleDateFormat(format).parse(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String encrypt(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(VECTOR.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String decrypt(String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(VECTOR.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted.replaceAll(" ","+")));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private static int workload = 6;

    public static String BcryptEncode(String password){

        String salt = BCrypt.gensalt(workload);
        return BCrypt.hashpw(password, salt);

    }

    public static Map profile(UserDTO user){
        Map<String, Object> userr = new HashMap<>();
        userr.put("id", user.getId());
        userr.put("username", user.getUsername());
        userr.put("password", "");
        userr.put("role", user.getLevelId().getName());
        Map<String, Object> data = new HashMap<>();
            data.put("displayName",user.getName()+" "+user.getLastName());
            data.put("photoURL",user.getPhoto());
            data.put("username",user.getUsername());
            Map<String, Object> settings = new HashMap<>();
                settings.put("customScrollbars",true);
                Map<String, Object> layout = new HashMap<>();
                    layout.put("style","layout1");
//                    Map<String, Object> config = new HashMap<>();
                settings.put("layout",layout);
                Map<String, Object> theme = new HashMap<>();
                    theme.put("main","defaultDark");
                    theme.put("navbar","defaultDark");
                    theme.put("toolbar","defaultDark");
                    theme.put("footer","defaultDark");
                settings.put("theme",theme);
            data.put("settings",settings);
        userr.put("data", data);
        return userr;
    }
}
